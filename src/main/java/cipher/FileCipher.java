package cipher;


import javafx.concurrent.Task;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;

/**
 * Created by diana on 19.01.2019.
 */
public class FileCipher{ //extends Task<Void> {
    private static final int BLOCK_SIZE = 8;

    private String input;
    private String output;
    private String key;
    private boolean encrypt;
    private EcbMode mode;

    public FileCipher(String input, String output, String key, boolean encrypt, EcbMode ecb) {
        this.input = input;
        this.output = output;
        this.key = key;
        this.encrypt = encrypt;
        this.mode = ecb;
    }

    public void cryptFile() throws IOException {

        try (FileChannel inChannel = FileChannel.open(Paths.get(input), StandardOpenOption.READ);
             FileChannel outChannel = FileChannel.open(Paths.get(output), StandardOpenOption.CREATE,
                     StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE)) {

            long inFileSize = inChannel.size(); // lungimea fisierului in bytes
            long inDataLen, outDataLen; // lungimea fisierului
            //verifica si calculeaza dimensiunea datelor din fis
            if(encrypt){
                inDataLen = inFileSize;
                outDataLen = (inDataLen + BLOCK_SIZE - 1) / BLOCK_SIZE * BLOCK_SIZE;

            }
            else{
                if(inFileSize==0){
                    throw new IOException("Input file is empty.");
                } else if (inFileSize % BLOCK_SIZE != 0) {
                    throw new IOException("Input file size is not a multiple of " + BLOCK_SIZE + ".");
                }
                inDataLen = inFileSize - BLOCK_SIZE; // Last block is the data size (encrypted)
                outDataLen = inDataLen;
            }
            processData(inChannel, inDataLen, outChannel, outDataLen, mode);

            // scrie/ citeste lungimea datelor
            if(encrypt)
            {
                System.out.print("in cryptFile 1 " + encrypt);
                writeDataLength(outChannel, inDataLen, mode);
            }
            else{
                System.out.print("in cryptFile 2" + encrypt);
                long dataSize = readDataLength(inChannel, mode);
                if (dataSize < 0 || dataSize > inDataLen || dataSize < inDataLen - BLOCK_SIZE + 1) {
                    throw new IOException("Input file is not a valid cryptogram (wrong file size)");
                }
                if (dataSize != outDataLen) {
                    outChannel.truncate(dataSize);
                }
            }

        }

    }
    private void processData(FileChannel inChannel, long inDataLen, FileChannel outChannel, long outDataLen,
                             EcbMode ecb) throws IOException {
        final int bufSize = 0x200000; // 2MB of buffer
        ByteBuffer buf = ByteBuffer.allocate(bufSize);
        long filePos = 0;
        while (filePos < inDataLen) {
            // Set progess
                                     //  updateProgress(filePos, inDataLen);
            // Read from input file into the buffer
            int bytesToRead = (int) Math.min(inDataLen - filePos, bufSize);
            buf.limit(bytesToRead);
            buf.position(0);
            int bytesRead = inChannel.read(buf);
            if (bytesRead != bytesToRead) {
                throw new IOException("Incomplete data chunk read from file.");
            }
            // Encrypt chunk
            int chunkLen = (bytesRead + BLOCK_SIZE - 1) / BLOCK_SIZE * BLOCK_SIZE; // Closest upper multiple of blockSize
            Arrays.fill(buf.array(), bytesRead, chunkLen, (byte) 0); // Fill the free space of the chunk with 0
            for (int pos = 0; pos < chunkLen; pos += BLOCK_SIZE) {
                ecb.crypt(buf.array(), pos); // Encrypt chunk with chosen operation mode
            }
            // Write buffer to output file
            System.out.print("in processData");
            int bytesToWrite = (int) Math.min(outDataLen - filePos, chunkLen);
            buf.limit(bytesToWrite);
            buf.position(0);
            int bytesWritten = outChannel.write(buf);
            if (bytesWritten != bytesToWrite) {
                throw new IOException("Incomplete data chunk written to file.");
            }
            filePos += chunkLen;
        }
       // writeDataLength(outChannel, inDataLen, mode);
    }
        private void writeDataLength(FileChannel outChannel, long dataLength, EcbMode ecb)
            throws IOException {
            // Package the dataLength into an 8-byte block
            byte[] block = packDataLength(dataLength);
            // Encrypt block
            ecb.crypt(block,0);
            System.out.print("in writeDataLength");
            System.out.print("ecb crypt : " + block);
            // Write block at the end of the file
            ByteBuffer buf = ByteBuffer.wrap(block);
            int bytesWritten = outChannel.write(buf);
            if (bytesWritten != BLOCK_SIZE) {
                throw new IOException("Error while writing data length suffix.");
            }
        }
    private long readDataLength(FileChannel channel, EcbMode ecb) throws IOException {
        // Get last block
        ByteBuffer buf = ByteBuffer.allocate(BLOCK_SIZE);
        int bytesRead = channel.read(buf);
        if (bytesRead != BLOCK_SIZE) {
            throw new IOException("Unable to read data length suffix.");
        }
        byte[] block = buf.array();
        // Decrypt block
        ecb.crypt(block,0);
        // Unpackage data length
        return unpackDataLength(block);
    }

    private static byte[] packDataLength(long size) {
        if (size > 0x1FFFFFFFFFFFL) { // 45 bits -> 32TB
            throw new IllegalArgumentException("File too long.");
        }
        byte[] b = new byte[BLOCK_SIZE];
        b[7] = (byte) (size << 3);
        b[6] = (byte) (size >> 5);
        b[5] = (byte) (size >> 13);
        b[4] = (byte) (size >> 21);
        b[3] = (byte) (size >> 29);
        b[2] = (byte) (size >> 37);
        return b;
    }

    private static long unpackDataLength(byte[] b) {
        if (b[0] != 0 || b[1] != 0 || (b[7] & 7) != 0) {
            return -1;
        }
        return (long) (b[7] & 0xFF) >> 3 |
                (long) (b[6] & 0xFF) << 5 |
                (long) (b[5] & 0xFF) << 13 |
                (long) (b[4] & 0xFF) << 21 |
                (long) (b[3] & 0xFF) << 29 |
                (long) (b[2] & 0xFF) << 37;
    }

 /*   @Override
    protected Void call() throws Exception {
        updateProgress(0, 1);
        cryptFile();
        updateProgress(1, 1);
        return null;
    }
   */
}
