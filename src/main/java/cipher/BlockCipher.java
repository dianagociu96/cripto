package cipher;

/**
 * Created by diana on 19.01.2019.
 */

public abstract class BlockCipher {

    private int keySize;
    private int blockSize;

    BlockCipher(int keySize, int blockSize) {
        this.keySize = keySize;
        this.blockSize = blockSize;
    }

    public int getBlockSize() {
        return blockSize;
    }

    protected abstract void setKey(byte[] key);

    protected void setKey(String charKey) {
        setKey(Operations.makeKey(charKey, keySize));
    }

    public abstract void crypt(byte[] data, int offset);

    public void crypt(byte[] data) {
        crypt(data, 0);
    }
}
