package cipher;

/**
 * Created by diana on 19.01.2019.
 */
public class EcbMode  {

    protected IdeaCipher idea;
    protected boolean encrypt;

    public EcbMode(boolean encrypt, String key) {
        this.encrypt=encrypt;
        this.idea= new IdeaCipher(key,encrypt);
    }

    protected void crypt(byte[] data, int pos) {
        idea.crypt(data, pos); // Encrypt / decrypt block
    }
}
